const BookHandlerDelete = require('./handlers/book-handler-delete');
const { BookHandlerGetAll, BookHandlerGetDetails } = require('./handlers/book-handler-get');
const BookHandlerPost = require('./handlers/book-handler-post');
const BookHandlerPut = require('./handlers/book-handler-put');

const routes = [
  // root
  {
    method: 'GET',
    path: '/',
    handler: () => '<h1> Welcome to Bookshelf API v1 </h1>',
  },
  // /books
  {
    method: 'GET',
    path: '/books',
    handler: BookHandlerGetAll,
  },
  {
    method: 'POST',
    path: '/books',
    handler: BookHandlerPost,
  },
  // /books/id
  {
    method: 'GET',
    path: '/books/{bookId}',
    handler: BookHandlerGetDetails,
  },
  {
    method: 'PUT',
    path: '/books/{bookId}',
    handler: BookHandlerPut,
  },
  {
    method: 'DELETE',
    path: '/books/{bookId}',
    handler: BookHandlerDelete,
  },
];

module.exports = routes;
