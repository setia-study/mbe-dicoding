const { bookshelf } = require('../models/books');

const BookHandlerGetAll = (req, head) => {
  const { query } = req;
  const booksResponse = [];
  let allBooks = [];

  if (bookshelf.size > 0) {
    bookshelf.forEach((book) => {
      allBooks.push(book);
    });

    // Filter by reading
    if (query.reading) {
      allBooks = allBooks.filter((book) => book.reading === (query.reading === '1'));
    }
    // Filter by finished
    if (query.finished) {
      allBooks = allBooks.filter((book) => book.finished === (query.finished === '1'));
    }
    // Filter by name
    if (query.name) {
      allBooks = allBooks.filter(
        (book) => book.name.toLowerCase().includes(query.name.toLowerCase()),
      );
    }

    allBooks.forEach((book) => {
      booksResponse.push({
        id: book.id,
        name: book.name,
        publisher: book.publisher,
      });
    });
  }

  const response = head.response({
    status: 'success',
    data: {
      books: booksResponse,
    },
  });
  response.code(200);

  return response;
};

const BookHandlerGetDetails = (req, head) => {
  const { bookId } = req.params;
  const searchedBook = bookshelf.get(bookId);
  let response;
  if (searchedBook) {
    response = head.response({
      status: 'success',
      data: {
        book: searchedBook,
      },
    });
    response.code(200);
  } else {
    response = head.response({
      status: 'fail',
      message: 'Buku tidak ditemukan',
    });
    response.code(404);
  }
  return response;
};

module.exports = { BookHandlerGetAll, BookHandlerGetDetails };
