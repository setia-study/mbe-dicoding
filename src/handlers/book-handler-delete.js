const { bookshelf } = require('../models/books');

const BookHandlerDelete = (req, head) => {
  const { bookId } = req.params;
  let errorFlag = 0;
  let searchedBook;
  let response;

  try {
    searchedBook = bookshelf.get(bookId);
    if (searchedBook) {
      bookshelf.delete(bookId);
    } else {
      errorFlag = 1;
    }
  } catch (error) {
    errorFlag = 2;
  }

  switch (errorFlag) {
    // SUCCESS
    case 0:
      response = head.response({
        status: 'success',
        message: 'Buku berhasil dihapus',
      });
      response.code(200);
      break;
    // ID NOT FOUND
    case 1:
      response = head.response({
        status: 'fail',
        message: 'Buku gagal dihapus. Id tidak ditemukan',
      });
      response.code(404);
      break;
    // OTHERS
    default:
      response = head.response({
        status: 'error',
        message: 'Buku gagal dihapus',
      });
      response.code(500);
      break;
  }

  return response;
};

module.exports = BookHandlerDelete;
